package com.mygdx.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer extends ScreenAdapter {
    public static final int BLOCK_SIZE = 40;
    
	private PacmanGame pacmanGame;
	private Pacman pacman;
	private World world;
	private Texture pacmanImg;
	private MazeRenderer mazeRenderer;
	
	private BitmapFont font;
	
	public WorldRenderer(PacmanGame pacmanGame, World world) {
        this.pacmanGame = pacmanGame;
        this.world = world;
        this.pacman = world.getPacman();
        mazeRenderer = new MazeRenderer(pacmanGame.batch, world.getMaze());
        pacmanImg = new Texture("pacman.png");
        font = new BitmapFont();
    }
	
	@Override
	public void render(float delta) {
    	Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mazeRenderer.render();
		SpriteBatch batch = pacmanGame.batch;
		batch.begin();
        Vector2 pos = pacman.getPosition();
        batch.draw(pacmanImg, pos.x - BLOCK_SIZE/2, PacmanGame.HEIGHT - pos.y - BLOCK_SIZE/2);
        font.draw(batch, " " + world.getScore(), 700, 60);
        batch.end();
	}
}
